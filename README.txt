
Menu access fix
====

Adds one administer permission per menu. You might want a certain role to be able
to edit just one menu. Now possible.

Hooks & alters
----

See menu_access_fix.api.php for details.

* You can alter the menu list so not ALL menus get a specific permission so the
  permissions page is kept clean.
* You can change the access result to specify access even more, or to determine
  access in a completely different way.
