<?php

/**
 * Implements hook_menu_access_fix_access().
 */
function hook_menu_access_fix_access($access, $op, $menu, $account) {
  // They can edit menu links, but not edit the menu itself (description).
  if ($op == 'edit one') {
    return FALSE;
  }
}

/**
 * Implements hook_menu_access_fix_menus_alter().
 */
function hook_menu_access_fix_menus_alter(&$menus) {
  // Unset the domain specific main menus, because we handle that access somewhere else.
  $domains = domain_domains();
  foreach ($domains as $did => $domain) {
    unset($menus['main-menu-' . $did]);
  }
}
